"""
django-notifier is an app to manage notification preferences and permissions
per user and per group.

To send a notification:

from notifier.shortcuts import create_notification, send_notification
create_notification('test-notif')
send_notification('test-notif', [user1, user2, ..])

"""

from django.apps import apps as django_apps
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings


VERSION = (0, 7, 0, 'f')  # following PEP 386
# VERSION = (0, 5, 2, "a", "1")


def get_version(short=False):
    version = "%s.%s" % (VERSION[0], VERSION[1])
    if short:
        return version
    if VERSION[2]:
        version = "%s.%s" % (version, VERSION[2])
    if VERSION[3] != "f":
        version = "%s%s%s" % (version, VERSION[3], VERSION[4])
    return version


def get_notification_model_label():
    return getattr(
        settings,
        'NOTIFIER_NOTIFICATION_MODEL',
        'notifier.Notification',
        )


def get_notification_model():
    """
    Returns the Notification model that is active in this project.
    """
    model = get_notification_model_label()
    try:
        return django_apps.get_model(model)
    except LookupError:
        raise ImproperlyConfigured(
            "NOTIFIER_NOTIFICATION_MODEL refers to a model '%s' "
            "that has not been installed" % model
        )


default_app_config = 'notifier.apps.NotifierConfig'
