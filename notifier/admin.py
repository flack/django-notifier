from django.contrib import admin

from . import models
from . import get_notification_model


class BackendAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_name', 'enabled')
    readonly_fields = ('name', 'display_name', 'description', 'klass')
    list_editable = ('enabled',)
admin.site.register(models.Backend, BackendAdmin)


class BackendConfigAdmin(admin.ModelAdmin):
    list_display = ('notification', 'backend', 'notify_default')
    readonly_fields = ('notification', 'backend')
    raw_id_fields = ('notification', 'backend')
    list_editable = ('notify_default',)
admin.site.register(models.BackendConfig, BackendConfigAdmin)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_name')
    prepopulated_fields = {'name': ('display_name',)}
admin.site.register(get_notification_model(), NotificationAdmin)


class GroupPrefsAdmin(admin.ModelAdmin):
    list_display = ('group', 'notification', 'backend', 'notify')
    list_editable = ('notify',)
    raw_id_fields = ('group',)
admin.site.register(models.GroupPrefs, GroupPrefsAdmin)


class UserPrefsAdmin(admin.ModelAdmin):
    list_display = ('user', 'notification', 'backend', 'notify')
    list_editable = ('notify',)
    raw_id_fields = ('user',)
    search_fields = ('=user__email', '=user__username')
admin.site.register(models.UserPrefs, UserPrefsAdmin)


class SentNotifcationAdmin(admin.ModelAdmin):
    list_display = ('user', 'notification', 'backend', 'success')
    readonly_fields = ('user', 'notification', 'backend', 'success')
    search_fields = ('=user__email', '=user__username')
admin.site.register(models.SentNotification, SentNotifcationAdmin)
