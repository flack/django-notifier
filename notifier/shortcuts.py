from collections import Iterable

import six

from django.contrib.auth.models import Group, Permission
from django.db.models.query import QuerySet
from django.contrib.auth import get_user_model

from .models import Backend, UserPrefs, BackendConfig
from . import get_notification_model
from . import conf as app_settings


def get_backend_configuration(notification, defaults=None):
    if defaults is None:
        defaults = {}

    default_settings = app_settings.BACKEND_CONFIGURATION_DEFAULTS
    if not default_settings:
        default_settings = {}

    ret = {}
    for bname in notification.backends.values_list("name", flat=True):
        # defaults
        value = defaults.get(bname, None)

        # default notification setting
        if value is None:
            setting = default_settings.get(notification.name, None)
            if setting is not None:
                value = setting.get(bname, None)

        # default global setting
        if value is None:
            setting = default_settings.get("__all__", None)
            if setting is not None:
                value = setting.get(bname, None)

        # False by default
        if value is None:
            value = False

        ret[bname] = value
    return ret


def create_notification(
    name, display_name=None,
    permissions=None, backends=None, public=True,
    backend_config=None,
    ):
    """
    Arguments

        :name: notification name, unique (string)
        :display_name: notification display name, can be non-unique (string)
        :permissions: list of permission names or objects
        :backends: list of backend names or objects
        :public: (boolean)
        :backend_config: backend config (dict or None)

    Returns
        Notification object
    """
    Notification = get_notification_model()

    if not display_name:
        display_name = name

    if backends:
        backends = _get_backend_queryset(backends)
    else:
        backends = Backend.objects.filter(enabled=True)

    if permissions:
        permissions = _get_permission_queryset(permissions)
    else:
        permissions = []

    try:
        n = Notification.objects.get(name=name)
    except Notification.DoesNotExist:
        n = Notification.objects.create(name=name, display_name=display_name,
            public=public)
        n.permissions.add(*permissions)
        n.backends.add(*backends)
    else:
        n.name = name
        n.display_name = display_name
        n.public = public
        try:
            n.permissions = permissions
        except TypeError as e:
            # newer django throws:
            # TypeError: Direct assignment to the forward side of a many-to-many set is prohibited. Use permissions.set() instead.
            n.permissions.set(permissions)
        try:
            n.backends = backends
        except TypeError as e:
            n.backends.set(backends)
        n.save()

    all_backends = {b.name: b for b in Backend.objects.iterator()}
    config = get_backend_configuration(n, backend_config)
    for bname, notify_default in config.items():
        b = all_backends.get(bname, None)
        if b is None:
            continue
        bc = BackendConfig.objects.get_or_create(
            notification=n,
            backend=all_backends[bname],
            defaults={'notify_default': notify_default},
            )

    return n


def send_notification(name_or_object, users, context=None):
    """
    Arguments

        :name_or_object: notification name or the Notification object (string)
        :users: user object or list of user objects
        :context: additional context for notification templates (dict)

    Returns

        None
    """
    Notification = get_notification_model()

    notification = name_or_object
    if not hasattr(name_or_object, "pk"):
        notification = Notification.objects.get(name=name_or_object)
    return notification.send(users, context)


def update_preferences(name, user, prefs_dict):
    """
    Arguments

        :name: notification name (string)
        :user: user or group object
        :prefs_dict: dict with backend obj or name as key with a boolean value.

            e.g. {'email': True, 'sms': False}, {email_backend_obj: True, sms_backend_obj: False}

    Returns

        dict with backend names that were created or updated. values that do not require change are skipped

        e.g. {'email': 'created', 'sms': updated}
    """
    Notification = get_notification_model()
    try:
        notification = Notification.objects.get(name=name)
    except Notification.DoesNotExist:
        return

    if isinstance(user, get_user_model()):
        return notification.update_user_prefs(user, prefs_dict)
    elif isinstance(user, Group):
        return notification.update_group_prefs(user, prefs_dict)


def clear_preferences(users):
    """
    Arguments

        :users: user object or list of user object

    Returns

        None
    """
    return UserPrefs.objects.remove_user_prefs(users)


def _get_permission_queryset(permissions):
    if (permissions and
            not isinstance(permissions, QuerySet)):
        if isinstance(permissions, Permission):
            permissions = [permissions]
        else:
            if isinstance(permissions, six.string_types):
                permissions = [permissions]
            elif isinstance(permissions, Iterable):
                if not all(isinstance(x, six.string_types) for x in permissions):
                    raise TypeError
            else:
                raise TypeError
            permissions = Permission.objects.filter(codename__in=permissions)

    return permissions


def _get_backend_queryset(backends):
    if (backends and
            not isinstance(backends, QuerySet)):
        if isinstance(backends, Backend):
            backends = [backends]
        else:
            if isinstance(backends, six.string_types):
                backends = [backends]
            elif isinstance(backends, Iterable):
                if not all(isinstance(x, six.string_types) for x in backends):
                    raise TypeError
            else:
                raise TypeError
            backends = Backend.objects.filter(name__in=backends)

    return backends


def set_notification_prefs(users, value=True, backend_names=None, notification_names=None):
    """Example usage:
    from yourapp import notifications
    from notifier.shortcuts import set_notification_prefs
    notification_names = ('name1', 'name2',)
    users = get_user_model().objects.all()
    set_notification_prefs(users=users, value=True, notification_names=notification_names)
    """
    Notification = get_notification_model()

    if notification_names is None:
        notification_names = Notification.objects.values_list("name", flat=True)
    if backend_names is None:
        backend_names = Backend.objects.values_list("name", flat=True)
    for nname in notification_names:
        prefs = {bname: value for bname in backend_names}
        for user in users:
            update_preferences(nname, user, prefs)
