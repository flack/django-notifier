from django.conf import settings


SENT_NOTIFICATIONS_CLEANUP_DAYS = getattr(
    settings, "SENT_NOTIFICATIONS_CLEANUP_DAYS", None
    )
