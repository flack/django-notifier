import datetime
from collections import Iterable

from django.utils import timezone
from django.contrib.auth import get_user_model

from celery.task import task
from celery.task.base import Task

from notifier.models import SentNotification
from notifier.shortcuts import send_notification
from notifier import get_notification_model

from . import conf as app_settings


class SendNotification(Task):
    ignore_result = True
    store_errors_even_if_ignored = True

    def run(self, name_or_object, user_pks, context=None, **kwargs):
        User = get_user_model()
        Notification = get_notification_model()

        if not isinstance(user_pks, Iterable):
            user_pks = [user_pks]
        users = User.objects.filter(pk__in=user_pks)

        return send_notification(name_or_object, users, context)


# run this periodically, every day or so
@task()
def delete_old_send_notifications(dry_run=False):
    logger = delete_old_notices.get_logger()

    if app_settings.SENT_NOTIFICATIONS_CLEANUP_DAYS:
        created_lt = timezone.now() - datetime.timedelta(
            days=app_settings.SENT_NOTIFICATIONS_CLEANUP_DAYS,
            )
        query = SentNotification.objects.filter(
            created__lt=created_lt,
            )
        idx = 0
        for idx, obj in enumerate(query.only("pk").iterator(), 1):
            if not dry_run:
                obj.delete()
        logger.info(
            "Deleted %s SentNotification objects created before: %s.",
            idx, created_lt,
            )
