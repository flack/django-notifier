from django.contrib.contenttypes.models import ContentType

from notifier import get_notification_model
from .tasks import SendNotification


def send_notification(name_or_object, users, context):
    content_object = context.pop('content_object', None)
    if content_object is not None:
        context.update({
            'content_type_id': ContentType.objects.get_for_model(content_object).pk,
            'content_object_id': content_object.pk,
        })

    if hasattr("users", "values_list"):
        user_pks = list(users.values_list("pk", flat=True))
    else:
        user_pks = []
        for user in users:
            if hasattr(user, "pk"):
                user_pks.append(user.pk)
            else:
                user_pks.append(user)

    return SendNotification.delay(name_or_object, user_pks, context)


