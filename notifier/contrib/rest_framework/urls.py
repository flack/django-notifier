from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(
    r'',
    views.NotificationSettingsViewSet,
    base_name="notification-settings",
    )


urlpatterns = router.urls
