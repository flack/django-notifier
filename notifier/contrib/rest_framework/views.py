from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins
from rest_framework.response import Response

from notifier.shortcuts import update_preferences
from notifier import get_notification_model


class NotificationSettingsViewSet(viewsets.GenericViewSet):
    """
    API endpoint for notification settings.
    """
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        Notification = get_notification_model()
        return Notification.objects.filter(public=True).order_by("name")

    def _get_data(self, user):
        settings = {}
        meta = {'notice_types': {}}
        backends_set = set()
        queryset = self.get_queryset()
        for notification in queryset:
            prefs = notification.get_user_prefs(user)
            settings[notification.name] = \
                {backend.name: val for backend, val in prefs.items()}
            backends = [backend for backend in prefs]
            backends_set = backends_set.union(set(backends))
            meta['notice_types'][notification.name] = {
                'display_name': notification.display_name,
                'backends': [b.name for b in backends],
            }
        meta['all_backends'] = [{
            'name': b.name,
            'display_name': b.display_name,
            'description': b.description
            } for b in backends_set]

        data = {
            'settings': settings,
            'meta': meta
        }

        return data

    def list(self, request, **kwargs):
        user = request.user
        data = self._get_data(user)
        return Response(data)

    def update(self, request, **kwargs):
        user = request.user
        data = request.data
        for notification, prefs in data.items():
            update_preferences(notification, user, prefs)

        data = self._get_data(user)['settings']
        return Response(data)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
