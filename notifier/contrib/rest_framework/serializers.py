from rest_framework import serializers

from notifier import get_notification_model


class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_notification_model()
        fields = ("id", "name", "display_name")
