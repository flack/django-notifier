from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework import status
from rest_framework import filters
from rest_framework.decorators import detail_route, list_route
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import NoticeWithContentObjectSerializer
from .. import get_notice_model


class NoticeViewSet(
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):
    """
    API endpoint that allows notices to be viewed.
    Subclass if you need.
    """
    queryset = get_notice_model().objects.all()
    serializer_class = NoticeWithContentObjectSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (
        DjangoFilterBackend,
        filters.OrderingFilter,
        )
    filter_fields = (
        "unseen", "notification",
        )
    ordering_fields = (
        "created_at", "unseen", "notification",
        )

    def _get_base_queryset(self):
        user = self.request.user
        qs = self.queryset\
            .filter(recipient=user)\
            .prefetch_related("notification")
        return qs

    def get_queryset(self):
        qs = self._get_base_queryset().order_by("-created_at")
        return qs

    @list_route(methods=['get'], url_path='unseen-count')
    def unseen_count(self, request, pk=None, **kwargs):
        qs = self._get_base_queryset()
        qs = self.filter_queryset(qs)
        count = qs.filter(unseen=True).count()
        return Response({'count': count})

    @list_route(methods=['POST'], url_path="mark-seen")
    def mark_seen(self, request, pk=None, **kwargs):
        notice_pks = request.data.get('notices', [])
        try:
            notices = self._get_base_queryset().filter(
                pk__in=notice_pks, unseen=True,
                )
        except TypeError:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        updated = notices.update(unseen=False)
        return Response({'updated': updated})
