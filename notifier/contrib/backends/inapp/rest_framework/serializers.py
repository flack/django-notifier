from rest_framework import serializers
from rest_framework.fields import ReadOnlyField

from notifier.contrib.rest_framework.serializers import NotificationSerializer
from .. import get_notice_model


class NoticeSerializer(serializers.ModelSerializer):
    """Subclass if you need."""
    notification_name = ReadOnlyField()
    notification = NotificationSerializer()

    class Meta:
        model = get_notice_model()
        exclude = ()


class NoticeWithContentObjectSerializer(NoticeSerializer):
    content_type_name = ReadOnlyField()
    # content_type_full_name = ReadOnlyField()
