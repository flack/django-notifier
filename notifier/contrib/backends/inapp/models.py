import collections

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
try:
    from django.contrib.contenttypes.fields import GenericForeignKey
except ImportError:
    from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.dispatch import receiver

from notifier import get_notification_model_label


class NoticeBase(models.Model):
    notification = models.ForeignKey(
        get_notification_model_label(),
        on_delete=models.CASCADE,
        verbose_name=_('notification'),
        )
    recipient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="received_notices",
        verbose_name=_("recipient")
        )
    message = models.TextField(_("message"))
    unseen = models.BooleanField(
        _("unseen"),
        default=True,
        )
    created_at = models.DateTimeField(
        _("created at"),
        auto_now_add=True,
        )

    class Meta:
        abstract = True


class NoticeBaseWithContentObject(NoticeBase):
    content_type = models.ForeignKey(
        ContentType,
        null=True, blank=True,
        on_delete=models.CASCADE,
        )
    content_object_id = models.PositiveIntegerField(
        null=True, blank=True,
        )
    content_object = GenericForeignKey(
        'content_type', 'content_object_id',
        )

    class Meta:
        abstract = True

    @property
    def content_type_name(self):
        if self.content_type:
            return self.content_type.name

    @property
    def content_type_full_name(self):
        if self.content_type:
            return ".".join([
                self.content_type.app_label,
                self.content_type.name
                ])


class Notice(NoticeBaseWithContentObject):

    class Meta(NoticeBaseWithContentObject.Meta):
        swappable = 'NOTIFIER_INAPP_NOTICE_MODEL'
