import datetime

from django.utils import timezone

from celery.task import task

from . import conf as app_settings
from . import get_notice_model


# run this periodically, every day or so
@task()
def delete_old_notices(dry_run=False):
    logger = delete_old_notices.get_logger()

    Notice = get_notice_model()

    if app_settings.SEEN_NOTIFICATIONS_CLEANUP_DAYS:
        created_lt = timezone.now() - datetime.timedelta(
            days=app_settings.SEEN_NOTIFICATIONS_CLEANUP_DAYS
            )
        query = Notice.objects.filter(
            unseen=False,
            created__lt=created_lt,
            )
        idx = 0
        for idx, obj in enumerate(query.only("pk").iterator(), 1):
            if not dry_run:
                obj.delete()
        logger.info(
            "Deleted %s seen in-app notification created before %s.",
            idx, created_lt,
            )

    if app_settings.UNSEEN_NOTIFICATIONS_CLEANUP_DAYS:
        created_lt = timezone.now() - datetime.timedelta(
            days=app_settings.UNSEEN_NOTIFICATIONS_CLEANUP_DAYS,
            )
        query = Notice.objects.filter(
            unseen=True,
            created__lt=created_lt
            )
        idx = 0
        for idx, obj in enumerate(query.only("pk").iterator(), 1):
            if not dry_run:
                obj.delete()
        logger.info(
            "Deleted %s unseen in-app notification created before %s.",
            idx, created_lt,
            )
