from django.template.loader import render_to_string

from notifier.backends import BaseBackend

from . import get_notice_model


class InAppBackend(BaseBackend):
    name = 'inapp'
    display_name = 'In app'
    description = 'In app notification backend'

    def __init__(self, notification, *args, **kwargs):
        super(InAppBackend, self).__init__(notification, *args, **kwargs)

        self.template = (
            'notifier/%s/%s.html' % (notification.name, self.name)
        )

    def create_instance(self, recipient):
        Notice = get_notice_model()

        message = render_to_string(self.template, self.context).strip()
        content_object = self.context.get('content_object')
        if content_object is None:
            content_type_id = self.context.get('content_type_id')
            content_object_id = self.context.get('content_object_id')

        notice = Notice(
            notification=self.notification,
            recipient=recipient,
            message=message,
            )
        if content_object is not None:
            notice.content_object = content_object
        else:
            notice.content_type_id = content_type_id
            notice.content_object_id = content_object_id

        notice = self.pre_save_instance(notice)
        notice.save()

    def pre_save_instance(self, instance):
        """Reimplement this in a subclass to make pre save modifications."""
        return instance

    def send(self, user, context=None):
        if hasattr(user, "is_active") and not user.is_active:
            # do not send to disabled users
            return False

        super(InAppBackend, self).send(user, context)
        instance = self.create_instance(recipient=user)
        return True
