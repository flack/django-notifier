from django.conf import settings


SEEN_NOTIFICATIONS_CLEANUP_DAYS = getattr(
    settings, "SEEN_NOTIFICATIONS_CLEANUP_DAYS", None
    )
UNSEEN_NOTIFICATIONS_CLEANUP_DAYS = getattr(
    settings, "UNSEEN_NOTIFICATIONS_CLEANUP_DAYS", None
    )
