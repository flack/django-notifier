from django.apps import AppConfig as AppConfigBase


class AppConfig(AppConfigBase):
    name = "notifier.contrib.backends.inapp"
    label = "notifier_inapp"
    verbose_name = "Notifier InApp backend"
