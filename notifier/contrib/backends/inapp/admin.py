from django.contrib import admin

from . import get_notice_model


class NoticeAdmin(admin.ModelAdmin):
    list_display = (
        "notification", "recipient", "message",
        "created_at", "unseen", "content_object"
        )
    raw_id_fields = ("recipient", "notification")


admin.site.register(get_notice_model(), NoticeAdmin)
