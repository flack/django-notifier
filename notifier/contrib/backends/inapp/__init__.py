from django.apps import apps as django_apps
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


def get_notice_model_label():
    return getattr(
        settings,
        'NOTIFIER_INAPP_NOTICE_MODEL',
        'notifier_inapp.Notice',
        )


def get_notice_model():
    """
    Returns the Notice model that is active in this project.
    """
    model = get_notice_model_label()
    try:
        return django_apps.get_model(model)
    except LookupError:
        raise ImproperlyConfigured(
            "NOTIFIER_INAPP_NOTICE_MODEL refers to a model '%s' "
            "that has not been installed" % model
        )


default_app_config = 'notifier.contrib.backends.inapp.apps.AppConfig'
