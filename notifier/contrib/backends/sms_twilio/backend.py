from django.template.loader import render_to_string
from django.core.exceptions import ImproperlyConfigured

from notifier.backends import BaseBackend

try:
    from twilio.rest import TwilioRestClient
except ImportError:
    raise ImproperlyConfigured(
            'Twilio is not installed. '
            )

from . import settings as app_settings


client = TwilioRestClient(
    app_settings.TWILIO_ACCOUNT_SID,
    app_settings.TWILIO_AUTH_TOKEN,
    )


class TwilioSMSBackend(BaseBackend):
    name = 'sms-twilio'
    display_name = 'SMS'
    description = 'Send via SMS using Twilio'

    def get_phone_of_user(self, user):
        return user.phone

    def get_message(self):
        message = render_to_string(self.template, self.context)
        return message

    def create_sms(self):
        sms = client.sms.messages.create(
            to=self.get_phone_of_user(user),
            from_=app_settings.TWILIO_FROM_NUMBER,
            body=self.get_message(),
        )
        return sms

    def send(self, user, context=None):
        if hasattr(user, "is_active") and not user.is_active:
            # do not send to disabled users
            return

        super(TwilioSMSBackend, self).send(user, context)
        sms = self.create_sms()
        return True
