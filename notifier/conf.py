from importlib import import_module

from django.conf import settings


BACKENDS = getattr(
    settings,
    'NOTIFIER_BACKENDS',
    ('notifier.backends.EmailBackend',)
)

# By default, we'll look in the <app>.notifications module to create any
# app-specific notifications. Override this in your settings module to
# look elsewhere
AUTO_CREATE_MODULE_NAME = getattr(
    settings,
    'NOTIFIER_AUTO_CREATE_MODULE_NAME',
    'notifications'
)


# Whether or not to record SentNotification objects
# Not doing so can improve performance, but also lets you store your
# own
CREATE_SENT_NOTIFICATIONS = getattr(
    settings,
    'NOTIFIER_CREATE_SENT_NOTIFICATIONS',
    True
)


USE_PYNLINER = getattr(
    settings,
    'NOTIFIER_USE_PYNLINER',
    False
    )


EMAIL_BACKEND_BCC = getattr(
    settings,
    'NOTIFIER_EMAIL_BACKEND_BCC',
    None,
    )


# {
#     '__all__': {
#         'email': True,
#     }
#     'notification-name1': {
#         'email': True,
#         'sms': False
#     },
#     'notification-name2': {
#         'email': False,  # this override __all__
#     }
# }
BACKEND_CONFIGURATION_DEFAULTS = getattr(
    settings,
    'NOTIFIER_BACKEND_CONFIGURATION_DEFAULTS',
    None,
    )


def get_backend_classes():
    return [
        getattr(import_module(mod), cls)
        for (mod, cls) in (backend.rsplit(".", 1)
        for backend in BACKENDS)
    ]
