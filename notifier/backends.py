from smtplib import SMTPException

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import get_connection, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.template import TemplateDoesNotExist
from django.core.exceptions import ImproperlyConfigured
from django.utils import translation


# Pynlinter installed?
try:
    from pynliner import Pynliner
except ImportError:
    Pynliner = False

from . import conf as app_settings


class BaseBackend(object):
    # Name of backend method associated with this class
    name = None
    display_name = None
    description = None

    def __init__(self, notification, *args, **kwargs):
        self.notification = notification
        self.template = ('/notifier/%s/%s.txt' % (notification.name, self.name))

    # Define how to send the notification
    def send(self, user, context=None):
        if not context:
            self.context = {}
        else:
            self.context = context

        self.context.update({
            'user': user,
            'site': Site.objects.get_current() if settings.SITE_ID else None,
        })


def send_mail(**kwargs):
    connection = kwargs.pop('connection', None) or get_connection(
        username=kwargs.pop('auth_user', None),
        password=kwargs.pop('auth_password', None),
        fail_silently=kwargs.pop('fail_silently', False),
        )
    kwargs['connection'] = connection
    html_message = kwargs.pop('html_message', None)
    mail = EmailMultiAlternatives(**kwargs)
    if html_message:
        mail.attach_alternative(html_message, 'text/html')
    return mail.send()


class EmailBackend(BaseBackend):
    name = 'email'
    display_name = 'Email'
    description = 'Send via email'

    def __init__(self, notification, *args, **kwargs):
        super(EmailBackend, self).__init__(notification, *args, **kwargs)
        self.get_templates()

    def get_templates(self):
        self.template_subject = (
            'notifier/%s/%s_subject.txt' % (self.notification.name, self.name)
             )
        self.template_message = (
            'notifier/%s/%s_message.txt' % (self.notification.name, self.name)
            )
        self.template_html_message = (
            'notifier/%s/%s_message.html' % (self.notification.name, self.name)
            )

    def get_subject(self):
        subject = render_to_string(self.template_subject, self.context)
        subject = ''.join(subject.splitlines())
        return subject

    def get_message(self):
        message = render_to_string(self.template_message, self.context)
        return message

    def get_html_message(self):
        try:
            html_message = render_to_string(
                self.template_html_message,
                self.context,
                )
        except TemplateDoesNotExist:
            return None
        if app_settings.USE_PYNLINER:
            if not Pynliner:
                raise ImproperlyConfigured(
                    "Your 'NOTIFIER_USE_PYNLINER' setting is "
                    "set to True but Pynliner can't be imported. "
                    "Please install it."
                    )
            else:
                html_message = Pynliner().from_string(html_message).run()
        return html_message

    def send_email(self, user):
        if hasattr(user, "is_active") and not user.is_active:
            # do not send emails to disabled users
            return False

        subject = self.get_subject()
        message = self.get_message()
        html_message = self.get_html_message()
        try:
            send_mail(
                subject=subject,
                body=message,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[user.email],
                bcc=app_settings.EMAIL_BACKEND_BCC,
                html_message=html_message,
                )
        except SMTPException:
            return False
        return True

    def send(self, user, context=None):
        super(EmailBackend, self).send(user, context)

        return self.send_email(user)


class TranslationBackendMixin(object):

    def get_language(self, user):
        if hasattr(user, "language"):
            return user.language
        return None

    def send(self, user, context=None):
        language = self.get_language(user)
        if language:
            curr_language = translation.get_language()
            translation.activate(language)
            ret = super(TranslationBackendMixin, self).send(user, context)
            translation.activate(curr_language)
            return ret
        return super(TranslationBackendMixin, self).send(user, context)
