# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('notifier', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.NOTIFIER_NOTIFICATION_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='userprefs',
            name='notification',
            field=models.ForeignKey(to=settings.NOTIFIER_NOTIFICATION_MODEL),
        ),
        migrations.AddField(
            model_name='userprefs',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='sentnotification',
            name='backend',
            field=models.ForeignKey(to='notifier.Backend'),
        ),
        migrations.AddField(
            model_name='sentnotification',
            name='notification',
            field=models.ForeignKey(to=settings.NOTIFIER_NOTIFICATION_MODEL),
        ),
        migrations.AddField(
            model_name='sentnotification',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='groupprefs',
            name='backend',
            field=models.ForeignKey(to='notifier.Backend'),
        ),
        migrations.AddField(
            model_name='groupprefs',
            name='group',
            field=models.ForeignKey(to='auth.Group'),
        ),
        migrations.AddField(
            model_name='groupprefs',
            name='notification',
            field=models.ForeignKey(to=settings.NOTIFIER_NOTIFICATION_MODEL),
        ),
        migrations.AddField(
            model_name='backendconfig',
            name='backend',
            field=models.ForeignKey(to='notifier.Backend'),
        ),
        migrations.AddField(
            model_name='backendconfig',
            name='notification',
            field=models.ForeignKey(to=settings.NOTIFIER_NOTIFICATION_MODEL),
        ),
        migrations.AddField(
            model_name='notification',
            name='backends',
            field=models.ManyToManyField(to='notifier.Backend', verbose_name='backends', blank=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='permissions',
            field=models.ManyToManyField(to='auth.Permission', verbose_name='permissions', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='userprefs',
            unique_together=set([('user', 'notification', 'backend')]),
        ),
        migrations.AlterUniqueTogether(
            name='groupprefs',
            unique_together=set([('group', 'notification', 'backend')]),
        ),
        migrations.AlterUniqueTogether(
            name='backendconfig',
            unique_together=set([('backend', 'notification')]),
        ),
    ]
