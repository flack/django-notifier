#!/usr/bin/env python
from setuptools import setup, find_packages

VERSION = __import__('notifier').get_version()

setup(
    name="django-notifier",
    version=VERSION,
    description=("User and Group Notifications for Django"),
    packages=find_packages(),
    include_package_data=True,
    license='Simplified BSD',
)
